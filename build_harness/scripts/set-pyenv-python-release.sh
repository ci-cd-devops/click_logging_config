#!/bin/bash
#
# Copyright (c) 2024 Russell Smiley
#
# This file is part of click_logging_config.
#
# You should have received a copy of the MIT License along with click_logging_config.
# If not, see <https://opensource.org/licenses/MIT>.

set -ex


set_pyenv_root() {
  [[ -z "${PYENV_ROOT}" ]] && {
    # Try pyenv is installed in ~/.pyenv
    [[ -f "~/.pyenv/bin/pyenv" ]] && {
        echo "~/.pyenv"
    } || {
      # Try to find pyenv in $PATH
      command -v pyenv || {
        echo "Could not find pyenv in ~/.pyenv/bin/pyenv or $PATH" >/dev/stderr
        exit 1
      }
    }
  } || {
    # Use the external definition
    echo "${PYENV_ROOT}"
  }
}


extract_venv_bin(){
  . ./build_harness/scripts/define-venv-bin.sh --source-only
  venv_bin=$(define_venv)
  echo "venv_bin=${venv_bin}"
}


set_pyenv_shell() {
  command -v pyenv >/dev/null || export PATH="${pyenv_root}/bin:${PATH}"
  eval "$(pyenv init -)"
  pyenv shell "${this_python_version}"
}


# diagnostic logging
echo "pwd=$(pwd)"

this_python_version="${1}"

[[ -z "${this_python_version}" ]] && {
  echo "Usage: $0 <python_version>"
  exit 1
}

extract_venv_bin
pyenv_root="$(set_pyenv_root)"
echo "pyenv_root=${pyenv_root}"
set_pyenv_shell

# log some diagnostics to stdout for pipeline debugging
python3 --version
"${venv_bin}/python3" --version
