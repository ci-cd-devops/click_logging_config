build-essential
git
libffi-dev
liblzma-dev
libncurses5-dev
libncursesw5-dev
libreadline-dev
libsqlite3-dev
libssl-dev
libbz2-dev
llvm
make
tk-dev
wget
xz-utils

cargo
rustc
