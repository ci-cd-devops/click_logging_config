#!/bin/bash
#
# Copyright (c) 2024 Russell Smiley
#
# This file is part of click_logging_config.
#
# You should have received a copy of the MIT License along with click_logging_config.
# If not, see <https://opensource.org/licenses/MIT>.

set -ex

define_venv() {
  [[ -d "/venv" ]] && {
    # venv directory is in the root directory in a pipeline container
    echo "/venv/bin"
  } || {
    [[ -d ".venv" ]] && {
      # venv directory is in the local directory when run locally
      echo ".venv/bin"
    } || {
      echo "Could not find venv directory" >/dev/stderr
      exit 1
    }
  }
}
