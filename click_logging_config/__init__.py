#  Copyright (c) 2022 Russell Smiley
#
#  This file is part of click_logging_config.

"""Quick and easy logging parameters for click commands."""

# NOTE: Cannot import from _click or _logging due to circular dependencies in
#       relation to pyproject.toml build-engine & dynamic version loading.
from ._version import __version__  # noqa: F401
